package com.example.appextrato.view


import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.appextrato.R
import com.example.appextrato.databinding.FragmentExtractBinding
import com.example.appextrato.viewmodel.ExtractViewModel
import kotlinx.android.synthetic.main.fragment_extract.*
import kotlinx.android.synthetic.main.item_installment.view.*

class ExtractFragment : Fragment() {

    private lateinit var viewModel: ExtractViewModel
    private lateinit var dataBinding: FragmentExtractBinding
    private lateinit var installmentsAdapter: InstallmentsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ExtractViewModel::class.java)
        installmentsAdapter = InstallmentsAdapter(arrayListOf(), viewModel)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dataBinding = DataBindingUtil.inflate<FragmentExtractBinding>(
            inflater, R.layout.fragment_extract, container, false
        ).apply {
            userReturned = viewModel.user.value
        }
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inst_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = installmentsAdapter
        }

        observeViewModel()
    }

    fun observeViewModel(){
        viewModel.user.observe(this, Observer{
            it?.let {
                dataBinding.userReturned = it
                main_container.visibility = View.VISIBLE
                inst_list.visibility = View.VISIBLE
                loadingView.visibility = View.GONE
                installmentsAdapter.updateList(it.installments)
            }

        })

        viewModel.loading.observe(this, Observer {
            it?.let{
                loadingView.visibility = if(it) View.VISIBLE else View.GONE
                if(it){
                    main_container.visibility = View.GONE
                    inst_list.visibility = View.GONE
                }
            }
        })

        viewModel.viewLine.observe(this, Observer {
            it?.let{
                if(viewModel.checked.value == true){
                    it.setBackgroundColor(Color.parseColor("#b3ccff"))
                    Toast.makeText(context, it.pastDue.text.toString(), Toast.LENGTH_SHORT).show()
                }
                else{
                    it.setBackgroundColor(Color.WHITE)
                }

            }
        })
    }



}
