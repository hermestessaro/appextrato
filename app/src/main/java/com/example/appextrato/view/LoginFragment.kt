package com.example.appextrato.view


import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.appextrato.MainActivity

import com.example.appextrato.R
import com.example.appextrato.databinding.FragmentLoginBinding
import com.example.appextrato.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment: Fragment() {
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(
            inflater, R.layout.fragment_login, container, false
        ).apply {
            loginViewModel = viewModel
            callback = object : Callback {
                override fun onClickCallback() {
                    viewModel.onClick()
                }
            }
        }
        viewModel.loginUser.observe(this, Observer {
            when{
                it.username.isNullOrEmpty() -> field1ET.error = "Campo 1 não pode ser vazio."
                it.password.isNullOrEmpty() -> field2ET.error = "Campo 2 não pode ser vazio."
                it.username != it.password -> field1ET.error = "Os campos não são iguais."
                else -> {
                    viewModel.password.value = null
                    viewModel.username.value = null
                    it.username = null
                    it.password = null
                    view?.let{ Navigation.findNavController(it).navigate(LoginFragmentDirections.actionExtract()) }
                }
            }
        })
        return binding.root
    }

    interface Callback{
        fun onClickCallback()
    }
}
