package com.example.appextrato.view

import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.appextrato.R
import com.example.appextrato.databinding.ItemInstallmentBinding
import com.example.appextrato.model.Installment
import com.example.appextrato.viewmodel.ExtractViewModel

class InstallmentsAdapter(val instList: ArrayList<Installment>, val viewModel: ExtractViewModel) : RecyclerView.Adapter<InstallmentsAdapter.InstallmentViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InstallmentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ItemInstallmentBinding>(
            inflater, R.layout.item_installment, parent, false
        ).apply {
            extractVM = viewModel
        }

        return InstallmentViewHolder(view)
    }

    override fun getItemCount() = instList.size

    override fun onBindViewHolder(holder: InstallmentViewHolder, position: Int) {
        val splitted = instList[position].installment_record.split("/")
        val customRecord = SpannableStringBuilder()
            .bold{splitted[0]}
            .append("/")
            .append(splitted[1])


        if(splitted[0] == splitted[1]){
            holder.view.pastDue.setTextColor(Color.parseColor("#FFC107"))
            holder.view.carnet.setTextColor(Color.parseColor("#FFC107"))
            holder.view.value.setTextColor(Color.parseColor("#FFC107"))
            holder.view.installmentRecord.apply{
                setTextColor(Color.parseColor("#FFC107"))
                setTypeface(null, Typeface.BOLD)
            }
        }
        holder.view.installment = instList[position]
        holder.view.installmentRecord.text = customRecord

    }

    fun updateList(newInstList: List<Installment>){
        instList.clear()
        instList.addAll(newInstList)
        notifyDataSetChanged()
    }

    class InstallmentViewHolder(var view: ItemInstallmentBinding): RecyclerView.ViewHolder(view.root)
}