package com.example.appextrato.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appextrato.model.LoginUser

class LoginViewModel(application: Application): AndroidViewModel(application) {

    var username = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    var loginUser = MutableLiveData<LoginUser>()

    fun onClick(){
        loginUser.value = LoginUser(username.value, password.value)
    }
}