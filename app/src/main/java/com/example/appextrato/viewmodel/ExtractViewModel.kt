package com.example.appextrato.viewmodel

import android.app.Application
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.appextrato.api.ApiService
import com.example.appextrato.model.User
import kotlinx.android.synthetic.main.item_installment.view.*
import kotlinx.coroutines.launch

class ExtractViewModel(application: Application): BaseViewModel(application) {

    val user = MutableLiveData<User>()
    val loading = MutableLiveData<Boolean>()
    val viewLine = MutableLiveData<View>()
    val checked = MutableLiveData<Boolean>()
    private val service = ApiService()

    init {
        fetchFromRemote()
    }

    private fun fetchFromRemote(){
        loading.value = true
        launch {
            val response = service.getExtracts()
            if(response.isSuccessful){
                apiCallback(response.body()!!.data)
            }
        }
    }

    private fun apiCallback(userApi: User){
        user.value = userApi
        loading.value = false
    }

    fun onChangeChecked(v: View){
        viewLine.value = v.parent as View
    }
}