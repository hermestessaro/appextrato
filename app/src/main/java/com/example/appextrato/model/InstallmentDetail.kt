package com.example.appextrato.model

import com.google.gson.annotations.SerializedName

data class InstallmentDetail(
    @SerializedName("overdue_days")
    val overdue_days: String,
    @SerializedName("overdue_date")
    val overdue_date: String,
    @SerializedName("original_value")
    val original_value: String,
    @SerializedName("value_diff")
    val value_diff: String,
    @SerializedName("total_value")
    val total_value: String,
    @SerializedName("store")
    val store: String
)