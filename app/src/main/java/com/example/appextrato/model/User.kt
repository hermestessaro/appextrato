package com.example.appextrato.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("name")
    val name: String,

    @SerializedName("total_overdue")
    val total_overdue: String,

    @SerializedName("total_due")
    val total_due: String,

    @SerializedName("installments")
    val installments: List<Installment>,

    @SerializedName("limits")
    val limits: Limits
)