package com.example.appextrato.model

import com.google.gson.annotations.SerializedName

data class Limits(
    @SerializedName("total_due")
    val total_due: String,
    @SerializedName("total")
    val total: String,
    @SerializedName("expent")
    val expent: String,
    @SerializedName("available")
    val available: String
)