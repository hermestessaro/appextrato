package com.example.appextrato.model

import com.google.gson.annotations.SerializedName

data class Installment(
    @SerializedName("past_due")
    val past_due: String,
    @SerializedName("carnet")
    val carnet: String,
    @SerializedName("installment")
    val installment_record: String,
    @SerializedName("value")
    val value: String,
    @SerializedName("detail")
    val detail: InstallmentDetail
)