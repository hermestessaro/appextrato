package com.example.appextrato.model

data class LoginUser(
    var username: String?,
    var password: String?
)