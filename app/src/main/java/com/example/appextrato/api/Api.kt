package com.example.appextrato.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("extract.php?pwd=123456")
    suspend fun getExtracts(): Response<ApiAnswer>
}