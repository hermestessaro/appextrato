package com.example.appextrato.api

import com.example.appextrato.model.User

class ApiAnswer(
    val code: Int,
    val status: String,
    val data: User
)